#BOF

FROM ubuntu:focal

# ubuntu
RUN apt-get update && env DEBIAN_FRONTEND=noninteractive apt-get install --assume-yes \
	curl \
        gettext-base \
        git-restore-mtime \
		jq \
        python3-pip \
		unzip \
		vim \
    cowsay


# python3
RUN pip3 install --upgrade \
	awscli \
	Jinja2 \
	j2cli \
	yq \
    cowsay


# eksctl
RUN curl --silent --location "https://github.com/weaveworks/eksctl/releases/latest/download/eksctl_$(uname -s)_amd64.tar.gz" | tar xz -C /tmp && mv -vi /tmp/eksctl /usr/local/bin
RUN eksctl version


# kubectl
RUN curl -LO "https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl" && chmod --verbose +x ./kubectl && mv --verbose --interactive ./kubectl /usr/local/bin/kubectl

# helm
RUN curl -LO https://get.helm.sh/helm-v3.4.1-linux-amd64.tar.gz
RUN tar xvfz helm-v3.4.1-linux-amd64.tar.gz
RUN install linux-amd64/helm /usr/local/bin

#helmfile
RUN curl -LO https://github.com/roboll/helmfile/releases/download/v0.138.1/helmfile_linux_amd64
RUN install helmfile_linux_amd64 /usr/local/bin/helmfile

# packer 
RUN curl -O https://releases.hashicorp.com/packer/1.7.0/packer_1.7.0_linux_amd64.zip
RUN unzip packer_1.7.0_linux_amd64.zip
RUN install packer /usr/local/bin/packer
RUN rm -v packer packer_1.7.0_linux_amd64.zip

# home, sweet home
RUN [ -d /local ] || mkdir -pv /local
WORKDIR /local

#EOF
